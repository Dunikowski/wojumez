<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */


$tiles_front_page_3 = get_field('tiles_front_page_3',5);
$main_text = get_field('main_text',$tiles_front_page_3);
$main_heading = get_field('heading',$tiles_front_page_3);
$main_title = get_the_title($tiles_front_page_3);
$tiles_loop = get_field('loop');
$section_statistic = get_field('section_statistics',10);
get_header(); ?>
<div class="category-posts-heading">
  <img class="bg-image" src="<?php 
  
  if(has_post_thumbnail($tiles_front_page_3)){
    echo get_the_post_thumbnail_url($tiles_front_page_3);
  } else {
     echo get_template_directory_uri()."/images/basic_bg.jpg"; 
  }
  ?>">
  <h1 class="title"><?php echo $main_title;?></h1>
</div>
<section id="primary" class="service primary">
  <div class="w-content">
    <div class="corner">
      <span><?php echo $main_title;?></span>
    </div>
    <?php if($main_heading):;?>
    <div class="title">
      <?php echo $main_heading;?>
    </div>
    <?php endif;?>
    <?php if($main_text):;?>
    <div class="text">
      <?php echo $main_text;?>
    </div>
    <?php endif;?>
    <?php if($tiles_loop):;?>
    <div class="tiles">
    <?php foreach($tiles_loop as $item_tiles):;?>
      <div class="w-item">
      <a href="<?php echo get_permalink($item_tiles['hiperlink']);?>" class="item" rel="nofollow">
        <img src="<?php echo $item_tiles['img_bg']['url'];?>" alt="<?php echo $item_tiles['img_bg']['url'];?>" >
        <?php if($item_tiles['heading']):;?>
          <div class="tiles-title">
            <?php echo $item_tiles['heading'];?>
          </div>
        <?php endif;?>
        <?php if($item_tiles['text_btn']):;?>
        <p class="cta-arrow"><?php echo $item_tiles['text_btn'];?></p>
          <?php endif;?>
        </a>
      </div>
      <?php endforeach;?>
    </div>
  </div>
  <?php endif;?>
  <!--
  <?php if($section_statistic):;?>
  <div class="w-statistic relative">
    <?php if($section_statistic['bg_img']):;?>
    <img class="bg-image" src="<?php  echo $section_statistic['bg_img']['url'];?>" alt="<?php echo $section_statistic['bg_img']['alt'];?>">
    <?php endif;?>
    <div class="w-content">
      <?php if($section_statistic['loop']):;?>
      <?php foreach($section_statistic['loop'] as $item_service):;?>
      <div class="item-statistic">
        <?php if($item_service['ilosc_'] || $item_service['tekst']):;?>
        <div class="corner-service">
          <div class="child-corner">
            <?php if($item_service['ilosc_']):;?>
            <p class="number">
              <?php echo $item_service['ilosc_'];?>
              <span class="plus"></span>
            </p>
            <?php if($item_service['tekst']):;?>
            <p class="text">
              <?php echo $item_service['tekst'];?>
            </p>
            <?php endif;?>
            <?php endif;?>
          </div>
        </div>
        <?php endif;?>
      </div>
      <?php endforeach;?>
      <?php endif;?>
    </div>
  </div>
  <?php endif;?>
-->
  <?php
			/* Start the Loop */
			if ( have_posts() ) :;?>
  <div class="w-posts w-content">
    <?php
			while ( have_posts() ) : the_post();
			$section_post_img = get_field('img',$post->ID);
				?>
    <div class="posts">
      <div class="info">
        <h2 class="heading"><a href="<?php echo get_permalink();?>"><?php the_title();?></a></h2>
        <div class="text">
        <?php echo wp_trim_words(wp_filter_nohtml_kses(the_content()),30);?>
        </div>
      </div>

      <a href="<?php echo get_permalink();?>" class="w-img relative">
        <img src="<?php echo 	$section_post_img['url'];?>" alt="<?php echo 	$section_post_img['alt'];?>" class="bg-image">
      </a>
    </div>
    <?php
			endwhile;
	?>
  </div>
  <?php
			the_posts_navigation();

		endif; ?>
</section><!-- #primary -->

<?php
get_footer();