<?php
/**
* Template Name: Home
 */
get_header();

  $_section_carousel = get_field('carousel');
  $_section_generally_description = get_field('description_of_activity');
  $tiles_front_page_1 = get_field('tiles_front_page_1');
  $tiles_front_page_1_heading = get_the_title($tiles_front_page_1);

  $tiles_front_page_2 = get_field('tiles_front_page_2');
  $tiles_front_page_2_heading = get_the_title($tiles_front_page_2);
 
  $tiles_front_page_3 = get_field('tiles_front_page_3');
  $tiles_front_page_3_heading = get_the_title($tiles_front_page_3);
 
?>

<?php if($_section_carousel):;?>
<section dir="rtl" class="w-carousel js-main-carousel">
  <?php foreach ( $_section_carousel as $item_carousel ): ;?>
  <div class="item">
    <?php if($item_carousel['picture']):;?>
    <img src="<?php  echo $item_carousel['picture']['url']; ?>" alt="<?php  echo $item_carousel['picture']['alt']; ?>" />
    <?php endif;?>

    <div class="w-content-carousel-text">
      <?php if($item_carousel['main_text']  || $item_carousel['auxiliary_text'] ):;?>

      <div class="w-content-title">
        <?php if($item_carousel['main_text']):;?>
        <p class="title">
          <?php  echo $item_carousel['main_text']; ?>
        </p>
        <?php endif;?>
        <?php if($item_carousel['auxiliary_text']):;?>
        <p class="text">
          <?php  echo $item_carousel['auxiliary_text']; ?>
        </p>
        <?php endif;?>
      </div>
      <?php endif;?>

      <?php if($item_carousel['group_left_btn']['show_btn'] || $item_carousel['group_right_btn']['show_btn']  ):;?>
      <div class="w-bottoms">
        <?php if($item_carousel['group_left_btn']['show_btn']):;?>
        <a href="<?php  echo get_permalink($item_carousel['group_left_btn']['hiperlink_left']); ?>" class="cta" rel="nofollow"><?php echo $item_carousel['group_left_btn']['text']; ?></a>
        <?php endif;?>
        <?php if($item_carousel['group_right_btn']['show_btn']):;?>
        <a href="<?php  echo get_permalink($item_carousel['group_right_btn']['hiperlink_right']); ?>" class="cta" rel="nofollow"><?php echo $item_carousel['group_right_btn']['text']; ?></a>
        <?php endif;?>
      </div>
      <?php endif;?>
    </div>
  </div>

  <?php endforeach ;?>
</section>
<?php endif;?>
<div class="tiles w-content">
<?php if($tiles_front_page_1):;?>
      <a href="<?php echo get_permalink($tiles_front_page_1);?>" class="item third" rel="nofollow">

      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="50" height="50" viewBox="0 0 795.863 795.863" 
        xml:space="preserve">
      <g>
        <g>
          <path d="M148.076,488.185c5.637,0,11.183-0.486,16.67-1.282l-2.185,267.654l-53.109,26.554h89.323h4.827h47.985l-47.985-25.931
            v-12.695V624.002l526.59-123.479l-1.508,149.414l-27.764,17.711h94.942l-35.383-19.313V274.137l-21.729-26.554l-37.831,8.258
            V97.904c0,0-14.482-94.155-207.619-82.082l-171.406,121.5c0,0,132.778,366.159,379.025,158.545v-6.285l19.406-5.469l11.976,9.338
            l-1.698,166.613L203.602,577.775V474.248c44.585-23.156,78.35-73.33,88.951-134.739l2.543-0.582
            c16.805,63.882,62.389,123.422,166.82,128.346c205.206,9.655,239.006-107.676,239.006-107.676
            c-277.63,176.348-400.753-172.428-400.753-172.428c-4.604,10.581-9.203,28.916-11.656,51.228
            c-19.558-75.499-74.99-130.024-140.438-130.024C66.297,108.372,0,193.394,0,298.278C0.001,403.164,66.298,488.185,148.076,488.185
            z M493.828,45.306c0,0,115.354-31.9,161.224,52.598C655.051,97.904,564.351,164.104,493.828,45.306z M148.076,152.017
            c62.994,0,114.033,65.48,114.033,146.261c0,2.904-0.188,5.738-0.34,8.605c-0.643,14.332-2.887,28.098-6.525,41.059
            c-5.924,20.931-15.709,39.25-27.857,54.504l-34.421-61.013c1.96-4.888,3.661-10.075,4.938-15.58
            c2.001-8.655,3.286-17.841,3.286-27.575c0-45.337-23.783-82.08-53.114-82.08c-7.735,0-15.048,2.697-21.673,7.279l-30.966-54.863
            C111.183,158.07,129.063,152.017,148.076,152.017z M66.579,196.071l35.289,62.528c-4.263,11.805-6.901,25.192-6.901,39.678
            c0,45.34,23.782,82.083,53.109,82.083c6.186,0,12.032-1.96,17.578-4.96c0.531-0.281,0.959-0.812,1.488-1.11l31.102,55.125
            c-10.297,6.472-21.387,11.055-33.156,13.318c-5.548,1.072-11.206,1.808-17.012,1.808c-62.996,0-114.029-65.481-114.029-146.265
            C34.047,258.48,46.454,222.439,66.579,196.071z"/>
        </g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      </svg>
        <?php if($tiles_front_page_1_heading):;?>
          <div class="tiles-title">
            <?php echo $tiles_front_page_1_heading;?>
          </div>
        <?php endif;?>

        <p class="cta-arrow">Sprawdź ofertę</p>
      </a>
      <?php endif;?>
      <?php if($tiles_front_page_2):;?>
      <a href="<?php echo get_permalink($tiles_front_page_2);?>" class="item third" rel="nofollow">

      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="60" height="60"
        viewBox="0 0 512 512"  xml:space="preserve">
      <g>
        <g>
          <path d="M110.933,287.834c-23.526,0-42.667,19.14-42.667,42.667c0,23.526,19.14,42.667,42.667,42.667
            c23.526,0,42.667-19.14,42.667-42.667C153.6,306.975,134.46,287.834,110.933,287.834z M110.933,356.101
            c-14.114,0-25.6-11.486-25.6-25.6s11.486-25.6,25.6-25.6s25.6,11.486,25.6,25.6S125.047,356.101,110.933,356.101z"/>
        </g>
      </g>
      <g>
        <g>
          <path d="M503.467,287.834v-51.2c0-3.669-2.347-6.938-5.837-8.098l-48.145-16.051c-18.133-30.711-43.059-59.938-98.142-69.589
            c-0.102-0.017-0.205-0.034-0.307-0.051c-101.726-14.063-178.099,8.883-227.132,68.224c-36.497,0.529-73.754,8.158-95.667,22.187
            c-11.349,7.27-18.082,19.925-18.714,33.391l-0.99,21.188c-4.71,0-8.533,3.823-8.533,8.533v34.133c0,4.71,3.823,8.533,8.533,8.533
            h34.133c4.736,0,8.525-3.891,8.533-8.627c0-0.043,0.009-0.085,0.009-0.137c0.128-32.879,26.812-59.503,59.725-59.503
            c32.99,0,59.733,26.743,59.733,59.733c-0.614,5.18,3.473,8.533,8.533,8.533h153.6c4.736,0,8.525-3.891,8.533-8.627
            c0-0.043,0.008-0.085,0.008-0.137c0.128-32.879,26.812-59.503,59.725-59.503c32.99,0,59.733,26.743,59.733,59.733
            c-0.614,5.18,3.473,8.533,8.533,8.533h34.133c4.71,0,8.533-3.823,8.533-8.533v-34.133
            C512,291.657,508.177,287.834,503.467,287.834z M273.067,206.768c0,2.355-1.911,4.267-4.267,4.267H157.517
            c-3.942,0-5.734-4.847-2.825-7.501c29.611-26.991,67.268-42.428,113.673-46.532c2.5-0.222,4.702,1.792,4.702,4.301V206.768z
            M375.586,206.81c-0.026,2.338-1.929,4.224-4.267,4.224H294.4c-2.355,0-4.267-1.911-4.267-4.267v-46.592
            c0-2.364,1.886-4.309,4.241-4.309c17.015-0.017,35.021,1.22,54.161,3.866c9.114,1.604,17.237,3.806,24.627,6.511
            c1.698,0.623,2.782,2.27,2.765,4.079L375.586,206.81z M420.122,211.034h-23.202c-2.372,0-4.292-1.937-4.267-4.309l0.222-23.125
            c0.034-3.524,4.053-5.436,6.895-3.354c9.225,6.741,16.896,14.771,23.731,23.885C425.634,206.972,423.671,211.034,420.122,211.034z
            "/>
        </g>
      </g>
      <g>
        <g>
          <path d="M401.067,287.834c-23.526,0-42.667,19.14-42.667,42.667c0,23.526,19.14,42.667,42.667,42.667
            c23.526,0,42.667-19.14,42.667-42.667C443.733,306.975,424.593,287.834,401.067,287.834z M401.067,356.101
            c-14.114,0-25.6-11.486-25.6-25.6s11.486-25.6,25.6-25.6c14.114,0,25.6,11.486,25.6,25.6S415.181,356.101,401.067,356.101z"/>
        </g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      </svg>
        <?php if($tiles_front_page_2_heading):;?>
          <div class="tiles-title">
            <?php echo $tiles_front_page_2_heading;?>
          </div>
        <?php endif;?>
      
          <p class="cta-arrow">Sprawdź ofertę</p>
      </a>
      <?php endif;?>
      <?php if($tiles_front_page_3):;?>
      <a href="<?php echo get_permalink($tiles_front_page_3);?>" class="item third" rel="nofollow">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50" height="45" viewBox="0 0 512.006 512.006" xml:space="preserve">
          <g>
            <g>
              <g>
                <path d="M508.885,205.784l-42.667-42.667c-2.005-2.005-4.715-3.115-7.552-3.115H448v-10.667c0-2.837-1.131-5.547-3.115-7.552
									l-85.333-85.333C342.443,39.342,313.771,10.67,256,10.67c-57.728,0-84.203,26.475-103.552,45.781
									c-3.051,3.051-3.968,7.637-2.304,11.627c1.643,3.989,5.547,6.592,9.856,6.592c86.037,0,96,36.523,96,64
									c0,22.827-12.928,40.213-21.184,49.067l69.696,69.696c18.859-12.757,45.653-27.819,58.155-26.709v14.613
									c0,2.837,1.131,5.547,3.115,7.552l53.333,53.333c2.005,1.984,4.715,3.115,7.552,3.115h0.363c2.965-0.085,5.739-1.408,7.659-3.627
									l74.667-85.333C513.067,216.131,512.853,209.774,508.885,205.784z" />
                <path d="M16.128,406.126C5.739,416.515,0,430.339,0,446.296c0,30.357,24.683,55.04,55.04,55.04
									c15.467,0,30.315-6.571,40.725-18.005L288.363,271.47l-68.779-68.8L16.128,406.126z" />
              </g>
            </g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
        </svg>
        <?php if($tiles_front_page_3_heading):;?>
          <div class="tiles-title">
            <?php echo $tiles_front_page_3_heading;?>
          </div>
        <?php endif;?>
     
        <p class="cta-arrow">Sprawdź ofertę</p>
  </a>
      <?php endif;?>
    </div>
<?php if($_section_generally_description):;?>
<section class="generally-description">
<?php if($_section_generally_description['bg_image']):;?>
  <img class="bg-image" src="<?php echo $_section_generally_description['bg_image']['url'];?>" alt="<?php echo $_section_generally_description['bg_image']['alt'];?>"/>
<?php endif;?>
<?php if($_section_generally_description['loop']):;?>
  <div class="w-content">
  
    <?php foreach ( $_section_generally_description['loop'] as $item_description ): ;?>
    <?php if($item_description):;?>
    <div class="item-description">
      <h2 class="heading"><?php  echo $item_description['title']; ?></h2>
      <div class="text"><?php  echo $item_description['text']; ?></div>
    </div>
    <?php endif;?>
    <?php endforeach ;?>
    <?php if($_section_generally_description['cta']):;?>
    <a class="cta-arrow" href="<?php echo $_section_generally_description['cta']['hiperlink'];?>"><?php  echo $_section_generally_description['cta']['text']; ?></a>
    <?php endif;?>
  </div>
  <?php endif;?>
</section>
<?php endif;?>

<div class="map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4206.56421033326!2d16.997231630259925!3d51.17783536145142!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470febabdf27323b%3A0x2a923b9a1bcf8e6c!2sWaniliowa%2036%2C%2051-180%20Wroc%C5%82aw!5e0!3m2!1sen!2spl!4v1574671796837!5m2!1sen!2spl"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>

    
<?php

get_footer();