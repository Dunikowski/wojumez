<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
<div class="category-posts-heading">
  <img class="bg-image" src="<?php  
  
  
    echo get_template_directory_uri()."/images/basic_bg.jpg"; 
  
  ;?>">
  <h1 class="title">ERROR 404</h1>
</div>
	<section  class="w-content primary">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				

				
					<p class="title">Strony nie znaleziono</p>

				
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php

get_footer();
