<?php
/**
 * * Template Name: Basic theme
 *
 */
$main_text = get_field('main_text');
$main_heading = get_field('heading');
$info_Basic_theme = get_field('group_services');
$section_statistic = get_field('section_statistics');
get_header(); ?>
<div class="category-posts-heading">
  <img class="bg-image" src="<?php  
  if(has_post_thumbnail()){
    echo get_the_post_thumbnail_url();
  } else {
    echo get_template_directory_uri()."/images/basic_bg.jpg"; 
  }
 
  ?>">
  <h1 class="title"><?php the_title();?></h1>
</div>
<section id="primary" class="primary">
 
  <div class="posts w-content">
    <div class="info">
    <div class="corner">
      <span><?php the_title();?></span>
    </div>
      <p class="heading"><?php echo $info_Basic_theme['heading'] ;?></hp>
      <div class="text">
        <?php echo$info_Basic_theme['krotki_opis'];?>
      </div>
    </div>

    <div href="<?php echo get_permalink();?>" class="w-img relative">
      <img src="<?php echo $info_Basic_theme['_img']['url'];?>" alt="<?php echo $info_Basic_theme['_img']['alt'];?>" class="bg-image">
    </div>
  </div>
  <?php if($section_statistic):;?>
  <div class="w-statistic relative">
    <?php if($section_statistic['bg_img']):;?>
    <img class="bg-image" src="<?php  echo $section_statistic['bg_img']['url'];?>" alt="<?php echo $section_statistic['bg_img']['alt'];?>">
    <?php endif;?>
    <div class="w-content">
      <?php if($section_statistic['loop']):;?>
      <?php foreach($section_statistic['loop'] as $item_service):;?>
      <div class="item-statistic">
        <?php if($item_service['ilosc_'] || $item_service['tekst']):;?>
        <div class="corner-service">
          <div class="child-corner">
            <?php if($item_service['ilosc_']):;?>
            <p class="number">
              <?php echo $item_service['ilosc_'];?>
              <span class="plus"></span>
            </p>
            <?php if($item_service['tekst']):;?>
            <p class="text">
              <?php echo $item_service['tekst'];?>
            </p>
            <?php endif;?>
            <?php endif;?>
          </div>
        </div>
        <?php endif;?>
      </div>
      <?php endforeach;?>
      <?php endif;?>
    </div>
  </div>
  <?php endif;?>
  <div class="w-content main-text">
   
    <?php if($main_heading):;?>
    <div class="title">
      <?php echo $main_heading;?>
    </div>
    <?php endif;?>
    <?php if($main_text):;?>
    <div class="text">
      <?php echo $main_text;?>
    </div>
    <?php endif;?>
  </div>
</section><!-- #primary -->

<?php
get_footer();