<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="title before-element">Niczego nie znaleziono</h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p class="text"><?php printf( wp_kses( __( 'Gotowy opublikować swój pierwszy post? <a href="%1$s">Zacznij tutaj</a>.', 'wp-bootstrap-starter' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p class="text">Przepraszamy, ale nic nie pasowało do wyszukiwanych haseł. Spróbuj ponownie, używając kilku różnych słów kluczowych.</p>
			<?php

		else : ?>
					
			<p class="text"><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'wp-bootstrap-starter' ); ?></p>
			<?php
			
		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
