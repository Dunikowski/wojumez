<?php
/**
* Template Name: Category machine
 */

$category_id = get_field('category');

get_header(); ?>
<div class="category-posts-heading">
  <img class="bg-image" src="<?php  
  
  if(has_post_thumbnail()){
    echo get_the_post_thumbnail_url();
  } else {
    echo get_template_directory_uri()."/images/basic_bg.jpg"; 
  }
  
  ;?>">
  <h1 class="title"><?php echo the_title();?></h1>
</div>
<section id="primary" class="service primary">
  <div class="w-content">
    <div class="corner">
      <span><?php echo the_title();?></span>
    </div>
  </div>

  <!--
  <?php if($section_statistic):;?>
  <div class="w-statistic relative">
    <?php if($section_statistic['bg_img']):;?>
    <img class="bg-image" src="<?php  echo $section_statistic['bg_img']['url'];?>" alt="<?php echo $section_statistic['bg_img']['alt'];?>">
    <?php endif;?>
    <div class="w-content">
      <?php if($section_statistic['loop']):;?>
      <?php foreach($section_statistic['loop'] as $item_service):;?>
      <div class="item-statistic">
        <?php if($item_service['ilosc_'] || $item_service['tekst']):;?>
        <div class="corner-service">
          <div class="child-corner">
            <?php if($item_service['ilosc_']):;?>
            <p class="number">
              <?php echo $item_service['ilosc_'];?>
              <span class="plus"></span>
            </p>
            <?php if($item_service['tekst']):;?>
            <p class="text">
              <?php echo $item_service['tekst'];?>
            </p>
            <?php endif;?>
            <?php endif;?>
          </div>
        </div>
        <?php endif;?>
      </div>
      <?php endforeach;?>
      <?php endif;?>
    </div>
  </div>
  <?php endif;?>
   -->
  <?php 
 $_args = array('post_type' => 'machine, ',  'cat' => $category_id );
$the_query_machines = new WP_Query( $_args ); 
?>
  <?php
			/* Start the Loop */
      if ( $the_query_machines->have_posts() ) :;
      ?>
  <div class="w-posts w-content">
    <?php	while ( $the_query_machines->have_posts() ) : $the_query_machines->the_post();
    $section_group_services = get_field('img',get_the_ID());
    ?>
    <div class="posts">
      <div class="info">
        <h2 class="heading"><a href="<?php echo get_permalink();?>"><?php the_title();?></a></h2>
        <div class="text">
          <?php echo wp_trim_words(wp_filter_nohtml_kses(the_content()),30);?>
        </div>
      </div>

      <a href="<?php echo get_permalink();?>" class="w-img relative">
        <img src="<?php echo $section_group_services['url'];?>" alt="<?php echo $section_group_services['alt'];?>" class="bg-image">
      </a>
    </div>
    <?php endwhile;?>
  </div>
  <?php
			the_posts_navigation();

    endif; 
    wp_reset_postdata();
    ?>
</section>
 
<!-- #primary -->
  
<?php
get_footer();