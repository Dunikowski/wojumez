<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

 $section_top_navbar_5 = get_field('top_navbar',5);
 $section_top_navbar = get_field('addresses_data',18);
 $logotype = get_field('logotyp',5)
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
<div class="w-top-navbar">
    <div class="w-content">
        <?php if($section_top_navbar):;?>
            <div class="w-address-bar">
            <?php if($section_top_navbar['phone']):;?>
                        <div class="w-phones">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="13" height="13" viewBox="0 0 348.077 348.077" 
                                xml:space="preserve">
                            <g>
                                <g>
                                    <g>
                                        <path d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076
                                            c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257
                                            c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194
                                            C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02
                                            C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876
                                            c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029
                                            C350.631,303.527,350.95,285.795,340.273,275.083z"/>
                                    </g>
                                </g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            </svg>

                            <a class="phone" href="tel:<?php echo preg_replace('/(\s|-)/i','',$section_top_navbar['phone']);?>" rel="nofollow"><?php echo trim($section_top_navbar['phone']);?></a>
                        </div>
                    <?php endif;?>
                    <?php if($section_top_navbar['address']):;?>
                        <div class="w-address">

                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="13" height="13" viewBox="0 0 491.582 491.582"
                                    xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M245.791,0C153.799,0,78.957,74.841,78.957,166.833c0,36.967,21.764,93.187,68.493,176.926
                                            c31.887,57.138,63.627,105.4,64.966,107.433l22.941,34.773c2.313,3.507,6.232,5.617,10.434,5.617s8.121-2.11,10.434-5.617
                                            l22.94-34.771c1.326-2.01,32.835-49.855,64.967-107.435c46.729-83.735,68.493-139.955,68.493-176.926
                                            C412.625,74.841,337.783,0,245.791,0z M322.302,331.576c-31.685,56.775-62.696,103.869-64.003,105.848l-12.508,18.959
                                            l-12.504-18.954c-1.314-1.995-32.563-49.511-64.007-105.853c-43.345-77.676-65.323-133.104-65.323-164.743
                                            C103.957,88.626,167.583,25,245.791,25s141.834,63.626,141.834,141.833C387.625,198.476,365.647,253.902,322.302,331.576z"/>
                                        <path d="M245.791,73.291c-51.005,0-92.5,41.496-92.5,92.5s41.495,92.5,92.5,92.5s92.5-41.496,92.5-92.5
                                            S296.796,73.291,245.791,73.291z M245.791,233.291c-37.22,0-67.5-30.28-67.5-67.5s30.28-67.5,67.5-67.5
                                            c37.221,0,67.5,30.28,67.5,67.5S283.012,233.291,245.791,233.291z"/>
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                </svg>
                            <?php echo trim($section_top_navbar['address']);?>
                        </div>
                    <?php endif;?>
                    <?php if($section_top_navbar['address']):;?>
                        <div class="w-working-time">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="13" height="13" viewBox="0 0 97.16 97.16"  xml:space="preserve"
                                >
                            <g>
                                <g>
                                    <path d="M48.58,0C21.793,0,0,21.793,0,48.58s21.793,48.58,48.58,48.58s48.58-21.793,48.58-48.58S75.367,0,48.58,0z M48.58,86.823
                                        c-21.087,0-38.244-17.155-38.244-38.243S27.493,10.337,48.58,10.337S86.824,27.492,86.824,48.58S69.667,86.823,48.58,86.823z"/>
                                    <path d="M73.898,47.08H52.066V20.83c0-2.209-1.791-4-4-4c-2.209,0-4,1.791-4,4v30.25c0,2.209,1.791,4,4,4h25.832
                                        c2.209,0,4-1.791,4-4S76.107,47.08,73.898,47.08z"/>
                                </g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            </svg>
                            <?php echo trim($section_top_navbar['working_time']);?>
                        </div>
                    <?php endif;?>
            </div>
            <?php if($section_top_navbar):;?>
            
            <a class="link-to" href="<?php echo get_the_permalink($section_top_navbar_5);?>" rel="follow"><?php echo trim(get_the_title($section_top_navbar_5));?></a>
            <?php endif;?>
            
        <?php endif;?>
    </div>
</div>
    <header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
    <div class="w-content">
       
            <div class="logotype">
                <a href="/">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 792.137 792.136" 
                            xml:space="preserve">
                        <g>
                            <g>
                                <path d="M293.054,660.519c8.189,0,14.859-6.67,14.859-14.863c0-8.201-6.67-14.87-14.859-14.87c-8.211,0-14.881,6.669-14.881,14.87
                                    C278.172,653.849,284.842,660.519,293.054,660.519z"/>
                                <path d="M193.239,658.753c7.229,0,13.115-5.872,13.115-13.1c0-7.23-5.885-13.104-13.115-13.104
                                    c-7.216,0-13.091,5.873-13.091,13.104C180.147,652.881,186.022,658.753,193.239,658.753z"/>
                                <path d="M397.102,658.753c7.232,0,13.117-5.872,13.117-13.1c0-7.23-5.885-13.104-13.117-13.104c-7.217,0-13.09,5.873-13.09,13.104
                                    C384.011,652.881,389.885,658.753,397.102,658.753z"/>
                                <path d="M83.519,658.753c7.229,0,13.103-5.872,13.103-13.1c0-7.23-5.874-13.104-13.103-13.104
                                    c-7.218,0-13.091,5.873-13.091,13.104C70.428,652.881,76.302,658.753,83.519,658.753z"/>
                                <path d="M530.365,699.805v-3.587c25.098-3.146,44.596-24.623,44.596-50.562c0-25.947-19.498-47.425-44.596-50.57v-3.58H44.597
                                    v3.58C19.485,598.232,0,619.708,0,645.656c0,25.939,19.485,47.418,44.597,50.562v3.587H530.365z M83.519,619.808
                                    c12.042,0,22.095,8.312,24.957,19.476h59.815c2.848-11.164,12.9-19.476,24.947-19.476c12.042,0,22.1,8.312,24.96,19.476h48.056
                                    c2.886-12.157,13.774-21.24,26.799-21.24c13.017,0,23.89,9.083,26.793,21.24h52.309c2.847-11.164,12.901-19.476,24.947-19.476
                                    c12.043,0,22.1,8.312,24.959,19.476h53.084c2.857-11.164,12.916-19.476,24.957-19.476c14.25,0,25.834,11.598,25.834,25.848
                                    c0,14.24-11.584,25.839-25.834,25.839c-12.041,0-22.1-8.311-24.957-19.47h-53.084c-2.857,11.159-12.916,19.47-24.959,19.47
                                    c-12.044,0-22.099-8.311-24.946-19.47h-52.309c-2.903,12.154-13.776,21.236-26.793,21.236c-13.024,0-23.913-9.082-26.799-21.236
                                    h-48.056c-2.86,11.159-12.918,19.47-24.96,19.47c-12.047,0-22.099-8.311-24.947-19.47h-59.815
                                    c-2.862,11.159-12.915,19.47-24.957,19.47c-14.25,0-25.832-11.599-25.832-25.839C57.688,631.404,69.269,619.808,83.519,619.808z"
                                    />
                                <path d="M500.102,658.753c7.219,0,13.092-5.872,13.092-13.1c0-7.23-5.873-13.104-13.092-13.104
                                    c-7.229,0-13.113,5.873-13.113,13.104C486.988,652.881,492.873,658.753,500.102,658.753z"/>
                                <path d="M486.988,434.757l-42.977-74.881H307.914v68.512l-75.208,3.187v-17.521l-100.339-1.593v15.928H96.622v45.113h136.084
                                    v18.593H96.622v58.292h390.366V434.757L486.988,434.757z M459.941,473.501H333.044v-91.074h95.043l31.854,45.537V473.501z"/>
                                <polygon points="426.492,269.119 557.096,170.375 557.096,114.628 397.102,210.189 333.044,340.79 426.492,340.79 		"/>
                                <polygon points="783.254,425.202 600.096,92.332 574.961,114.628 574.961,160.817 738.658,425.202 		"/>
                                <path d="M749.809,455.126c1.068,92.65-86.006,95.26-86.006,95.26c176.812,69.446,119.451-95.26,119.451-95.26H749.809
                                    L749.809,455.126z"/>
                            </g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        </svg> 
                        <?php if($logotype['name']):;?>
                            <span class="name"><?php echo $logotype['name'];?></span>
                        <?php endif;?>
                        <?php if($logotype['heading']):;?>
                            <span class="heading"><?php echo $logotype['heading'];?></span>
                        <?php endif;?>
                </a>
            </div>
        
    
            <nav class="navbar navbar-expand-xl p-0">
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                wp_nav_menu(array(
                'theme_location'    => 'primary',
                'container'       => 'div',
                'container_id'    => 'main-nav',
                'container_class' => 'collapse navbar-collapse',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 3,
                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>

            </nav>
        
    </div>
        
	</header><!-- #masthead -->
    

                <?php endif; ?>