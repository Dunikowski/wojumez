(function($) {

  let wojumez = {};
  wojumez.body = $('body');
  wojumez.widthWindow = $(window).width();

  if (wojumez.body.length > 0 && wojumez.body.hasClass('page-template-front-page')) {
    wojumez.body.find('.js-main-carousel').slick({
      rtl: true,
      lazyLoad: 'ondemand',
      autoplay: true,
      speed: 1500,
      autoplaySpeed: 3000,
      arrows: false,
      dots: true,
      responsive: [
        {
          breakpoint: 1050,
          settings: {
            speed: 1000,
            autoplaySpeed: 2000,
          }
        },
        {
          breakpoint: 500,
          settings: {
            speed: 600
          }
        }
      ]
    });
  }

  let MobileMenu = {};
  MobileMenu.init = function() {
   

    const _nav = $('#main-nav');
    if (_nav.length > 0 && _nav.find("span.close").length == 0) {
      _nav.prepend("<span class='close'></span>");
    }
    const _span = _nav.find("span.close");
    if (_span.length > 0) {
      _span.on("click", function() {
        _nav.siblings('button.navbar-toggler').click();
      });
    }
  }
  MobileMenu.removeBTN = function () {
    const _nav = $('#main-nav');
    const _btn =_nav.find("span.close");
    if (_nav.length > 0 && _btn.length > 0) {
      _btn.remove();
    }
    return;
  }

  if (wojumez.widthWindow < 1200) {
    MobileMenu.init();
  }

  let setBanner = {};
  setBanner.init = function() {
    const i = $('.js-banner').find('.w-content-banner');
    
    if (i.length > 0) {
      i.removeAttr('style');
      const height_half = i.outerHeight() * 0.5;
      i.css({
        top: -height_half
      });
    }
  }

  if (wojumez.widthWindow > 600) {
    setBanner.init();
  }

  $(window).on('resize', function() {
    wojumez.widthWindow = $(window).width();
    if (wojumez.widthWindow > 600) {
      setBanner.init();
    }

    if (wojumez.widthWindow < 1200) {
      MobileMenu.init();
    } else {
      MobileMenu.removeBTN();
    }
  });
})(jQuery)