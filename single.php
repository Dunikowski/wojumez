<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */
$post_thumbnail = get_the_post_thumbnail($post->ID); 
$_img = get_field('img');
get_header(); ?>

<section class="rental">
	<div class="category-posts-heading">
	<img class="bg-image" src="<?php  
	
	if(has_post_thumbnail()){
		echo get_the_post_thumbnail_url();
	} else {
		echo get_template_directory_uri()."/images/basic_bg.jpg"; 
	}
	
	;?>">
	<h1 class="title"><?php echo the_title();?></h1>
	</div>
  <div class="service primary">
  <div class="w-posts w-content">
    
    <div class="posts">
	<div href="<?php echo get_permalink();?>" class="w-img relative">
        <img src="<?php echo $_img ['url'];?>" alt="<?php echo $_img ['alt'];?>" class="bg-image">
	</div>
      <div class="info">
        <p class="heading"><?php the_title();?></p>
        <div class="text">
          <?php the_content();?>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php

get_footer();