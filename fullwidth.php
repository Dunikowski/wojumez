<?php
/**
* Template Name: Full Width
 */

get_header(); ?>
<div class="category-posts-heading">
  <img class="bg-image" src="<?php  
  if(has_post_thumbnail()){
    echo get_the_post_thumbnail_url();
  } else {
    echo get_template_directory_uri()."/images/basic_bg.jpg"; 
  }
 
  ?>">
  <h1 class="title"><?php the_title();?></h1>
</div>
	<div id="primary" class="w-content primary">
		

	<div class="corner">
      <span><?php the_title();?></span>
    </div>
			<div class="text">
			<?php
			the_content();
			?>
			</div>

</div>

<?php
get_footer();
